//
//  Const.swift
//  CoreData2
//
//  Created by 金鑫 on 2020/6/6.
//  Copyright © 2020 金鑫. All rights reserved.
//

import Foundation
import UIKit

let kScreenWidth: CGFloat = UIScreen.main.bounds.size.width
let kScreenHeight: CGFloat = UIScreen.main.bounds.size.height

let kMargin: CGFloat = 10
let kButtonW: CGFloat = 100
