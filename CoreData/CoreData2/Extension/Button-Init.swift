//
//  Button-Init.swift
//  CoreData2
//
//  Created by 金鑫 on 2020/6/6.
//  Copyright © 2020 金鑫. All rights reserved.
//

import Foundation
import UIKit

extension UIButton {
    
    public convenience init(title: String!, target: Any!, action: Selector!) {
        self.init(type: .custom)
        
        self.setTitle(title, for: .normal)
        self.titleLabel?.font = UIFont.systemFont(ofSize: 16)
        self.setTitleColor(UIColor.black, for: .normal)
        self.setTitleColor(UIColor.gray, for: .highlighted)
        self.addTarget(target, action: action, for: .touchUpInside)
        self.sizeToFit()
        
        self.layer.borderWidth = 1;
        self.layer.cornerRadius = 10.0;
//        self.layer.masksToBounds = true;
        self.layer.borderColor = UIColor.black.cgColor;
    }
}
