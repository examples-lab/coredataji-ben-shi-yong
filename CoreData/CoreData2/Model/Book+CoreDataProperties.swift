//
//  Book+CoreDataProperties.swift
//  CoreData2
//
//  Created by 金鑫 on 2020/6/8.
//  Copyright © 2020 金鑫. All rights reserved.
//
//

import Foundation
import CoreData


extension Book {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Book> {
        return NSFetchRequest<Book>(entityName: "Book")
    }

    @NSManaged public var price: Float
    @NSManaged public var title: String?
    @NSManaged public var owner: Student?

}
