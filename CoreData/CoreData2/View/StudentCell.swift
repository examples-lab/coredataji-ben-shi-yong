//
//  StudentCell.swift
//  CoreData2
//
//  Created by 金鑫 on 2020/6/7.
//  Copyright © 2020 金鑫. All rights reserved.
//

import UIKit

class StudentCell: UITableViewCell {
    
    @IBOutlet weak var userIconImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var sexLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var heightLabel: UILabel!
    
    open var student: Student? {
        didSet {
            guard let student = student else { return }
            // TODO: jinxin 完成cell
            nameLabel.text = student.name
            sexLabel.text = student.sex
            ageLabel.text = String(student.age)
            idLabel.text = String(student.id)
            heightLabel.text = String(student.height)
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
}
