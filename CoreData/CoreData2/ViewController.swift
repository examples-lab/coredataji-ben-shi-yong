//
//  ViewController.swift
//  CoreData2
//
//  Created by 金鑫 on 2020/6/5.
//  Copyright © 2020 金鑫. All rights reserved.
//

import UIKit
import CoreData
import SnapKit

class ViewController: UIViewController {
    
    let StudentCellId = "StudentCell"
    
    // CoreData Container
    var context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    // MARK: containerView
    lazy var insertBtn: UIButton! = {
        let insertBtn = UIButton(title: "插入", target: self, action: #selector(insertInfo))
        return insertBtn
    }()
    lazy var sortBtn: UIButton! = {
        let sortBtn = UIButton(title: "排序", target: self, action: #selector(sortInfo))
        return sortBtn
    }()
    lazy var searchBtn: UIButton! = {
        let searchBtn = UIButton(title: "查找", target: self, action: #selector(searchInfo))
        return searchBtn
    }()
    lazy var deleteBtn: UIButton! = {
        let deleteBtn = UIButton(title: "全部删除", target: self, action: #selector(deleteAll))
        return deleteBtn
    }()
    
    lazy var containerView: UIView! = {
        let containerView = UIView()
        containerView.backgroundColor = UIColor(red: 220/225, green: 220/225, blue: 220/225, alpha: 1)
        containerView.layer.borderWidth = 0.5
        containerView.layer.borderColor = UIColor.black.cgColor
        containerView.layer.cornerRadius = 10
//        containerView.layer.masksToBounds = true
        
        // subViews
        containerView.addSubview(insertBtn)
        containerView.addSubview(sortBtn)
        containerView.addSubview(searchBtn)
        containerView.addSubview(deleteBtn)
        
//        containerView.addSubview(UILabel)
        
        return containerView
    }()
    
    // MARK: -TableView
    lazy var tableView: UITableView! = {
        let tableView = UITableView()
        tableView.dataSource = self;
        tableView.delegate = self;
        tableView.backgroundColor = UIColor(red: 245/255, green: 245/255, blue: 245/255, alpha: 1)
        tableView.rowHeight = 60
        tableView.separatorInset = UIEdgeInsets(top: 30, left: 0, bottom: 30, right: 0)
        tableView.separatorStyle = .singleLine
        
        tableView.register(UINib.init(nibName: "StudentCell", bundle: nil), forCellReuseIdentifier: StudentCellId)
        
        return tableView
    }()
    
    // MARK: -ModelData
    lazy var dataArray: [Student] = {
        let dataArray = CoreDataTool.shared.fetchAll()
        return dataArray
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        
        layoutUI()
        
//        let _ = dataArray

//        print(NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.applicationDirectory, FileManager.SearchPathDomainMask.userDomainMask, true))
        
    }
}

// MARK: -Setup UI
extension ViewController {
    
    private func setupUI() {
        view.addSubview(containerView)
        view.addSubview(tableView)
    }
    
    private func layoutUI() {
        containerView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(41)
            make.left.equalToSuperview().offset(kMargin)
            make.right.equalToSuperview().offset(-kMargin)
            make.centerX.equalToSuperview()
        }
        
        insertBtn.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(kMargin * 2)
            make.left.equalToSuperview().offset(kMargin * 4)
            make.width.equalTo(kButtonW)
        }
        sortBtn.snp.makeConstraints { (make) in
            make.top.equalTo(insertBtn)
            make.right.equalToSuperview().offset(-kMargin * 4)
            make.width.equalTo(insertBtn)
        }
        searchBtn.snp.makeConstraints { (make) in
            make.top.equalTo(insertBtn.snp.bottom).offset(30)
            make.left.equalTo(insertBtn)
            make.bottom.equalToSuperview().offset(-kMargin * 2)
            make.width.equalTo(sortBtn)
        }
        deleteBtn.snp.makeConstraints { (make) in
            make.top.equalTo(searchBtn)
            make.right.equalTo(sortBtn)
            make.width.equalTo(searchBtn)
        }
        
        // tableView
        tableView.snp.makeConstraints { (make) in
            make.top.equalTo(containerView.snp.bottom).offset(kMargin)
            make.left.equalTo(containerView)
            make.right.equalTo(containerView)
            make.bottom.equalToSuperview().offset(-41)
        }
    }
}

// MARK: -Actions For Button
extension ViewController {
    static var clickCountForSort: Int = 0
    
    @objc private func insertInfo() {
        print("insert >>>")
        
        guard let student = CoreDataTool.shared.insert() else { return }
        dataArray.append(student)
//        let indexPath = IndexPath(index: dataArray.count - 1)
        let indexPath = IndexPath(row: dataArray.count - 1, section: 0)
        tableView.insertRows(at: [indexPath], with: .automatic)
    }
       
    @objc private func sortInfo(button: UIButton) {
        print("sort >>>")
        
        ViewController.clickCountForSort += 1
        let mode: CDSortMode：Int = (ViewController.clickCountForSort % 2) == 1 ? CDSortMode：Int.asc : CDSortMode：Int.desc
        dataArray = CoreDataTool.shared.sort(sortMode: mode)
        tableView.reloadData()
        
    }
    
    @objc private func searchInfo() {
        print("search >>>")
        
        guard let result = CoreDataTool.shared.search(searchCondition: "age <= 25") else { return }
        dataArray = result
        tableView.reloadData()
    }
    
    @objc private func modifyInfo(indexPath: IndexPath) {
        print("modify >>>")
        
        let entity = CoreDataTool.shared.modify(indexPath: indexPath)
        dataArray[indexPath.row] = entity
        tableView.reloadRows(at: [indexPath], with: .automatic)
    }
    
    @objc private func deleteInfo(indexPath: IndexPath) {
        print("delete >>>")
        
        CoreDataTool.shared.delete(indexPath: indexPath)
        dataArray.remove(at: indexPath.row)
        tableView.deleteRows(at: [indexPath], with: .automatic)
    }
    
    @objc private func deleteAll() {
        print("deleteAll >>>")
        
        CoreDataTool.shared.deleteAll()
        dataArray.removeAll()
        tableView.reloadData()
    }
    
}

// MARK: -TableViewDataSource
extension ViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: StudentCellId) as! StudentCell
        
        cell.student = dataArray[indexPath.row]
        
        return cell
    }
}

// MARK: -TableViewDelegate
extension ViewController: UITableViewDelegate {
    
    // MARK: 左滑
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let deleteAction: UIContextualAction = UIContextualAction(style: .destructive, title: "删除") { [weak self] (action, sourceView, completionHandler) in
            self!.deleteInfo(indexPath: indexPath)
            print("删除了")
            
        }
        deleteAction.backgroundColor = UIColor.red
        
        let modifyAction: UIContextualAction = UIContextualAction(style: .normal, title: "修改") { [weak self] (action, sourceView, completionHandler) in
            self!.modifyInfo(indexPath: indexPath)
            print("修改了")
        }
        modifyAction.backgroundColor = UIColor.orange
        
        let actions: [UIContextualAction] = [deleteAction, modifyAction]
        
        let configuration: UISwipeActionsConfiguration = UISwipeActionsConfiguration(actions: actions)
        
        configuration.performsFirstActionWithFullSwipe = false
        
        return configuration
    }
    
}
