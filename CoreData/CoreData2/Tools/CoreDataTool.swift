//
//  CoreDataTool.swift
//  CoreData2
//
//  Created by 金鑫 on 2020/6/7.
//  Copyright © 2020 金鑫. All rights reserved.
//

import UIKit
import CoreData

public enum CDSortMode：Int {
    case asc
    case desc
}

class CoreDataTool: NSObject {
    
    // FIXME: jinxin test
    let names: [String] = ["Adam", "Eva", "Eliy", "Snake"]
    let sexes: [String] = ["M", "F"]
    
    static let shared: CoreDataTool = CoreDataTool()
    
    static let context: NSManagedObjectContext! = {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let context = appDelegate.persistentContainer.viewContext
        
        return context
    }()
    
//    func entity(entityName: String) -> Any? {
//        guard let anyClass = NSClassFromString(entityName) else { return nil }
//
//        guard let entity = NSEntityDescription.entity(forEntityName: entityName, in: CoreDataTool.context) else { return nil }
//
//        let obj = anyClass.init(entity: entity, insertInto: CoreDataTool.context)
//
//        return obj
//    }
    
    func insert() -> Student? {
        
        guard let entity = NSEntityDescription.entity(forEntityName: "Student", in: CoreDataTool.context) else { return nil }
        
        let student = Student(entity: entity, insertInto: CoreDataTool.context)
//        let student = CoreDataTool.shared.entity(entityName: "Student") as! Student
        student.id = Int16(arc4random() % 20 + 1)
        student.name = names[Int(arc4random()) % names.count]
        student.sex = sexes[Int(arc4random()) % sexes.count]
        student.age = Int16(20 + arc4random() % 21)
        student.height = Int16(160 + arc4random() % 31)
        
        if CoreDataTool.context.hasChanges {
            do {
                try CoreDataTool.context.save()
            } catch {
                print("Failed saving")
            }
        }
        
        return student
    }
    
    func sort(sortMode: CDSortMode：Int) -> [Student] {
        var students = [Student]()
        
        let isAscending: Bool = sortMode == .asc ? true : false
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Student")
        
        let sort = NSSortDescriptor(key: "id", ascending: isAscending)
        request.sortDescriptors = [sort]
        do {
            students = try CoreDataTool.context.fetch(request) as! [Student]
            print(students)
        } catch {
            print("Faiiled Fetching")
        }
        
        return students
    }
    
    func search(searchCondition: String) -> [Student]? {
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Student")
        
        let predicate = NSPredicate.init(format: searchCondition)
        
        request.predicate = predicate
        
        let students = try? CoreDataTool.context.fetch(request) as? [Student]
        
        print(students as Any)
        
        return students
    }
    
    func modify(indexPath: IndexPath) -> Student {
        let entities = fetchAll()
        
        let entity = entities[indexPath.row]
        
        entity.id = Int16(arc4random() % 20 + 1)
        entity.name = names[Int(arc4random()) % names.count]
        entity.sex = sexes[Int(arc4random()) % sexes.count]
        entity.age = Int16(20 + arc4random() % 21)
        entity.height = Int16(160 + arc4random() % 31)
        
        if CoreDataTool.context.hasChanges {
            do {
                try CoreDataTool.context.save()
            } catch {
                print("Failed saving")
            }
        }
        
        return entity
    }
    
    func delete(indexPath: IndexPath) {
        let entities = fetchAll()
        
        CoreDataTool.context.delete(entities[indexPath.row])
        
        if CoreDataTool.context.hasChanges {
            do {
                try CoreDataTool.context.save()
            } catch {
                print("Failed saving")
            }
        }
    }
    
    func deleteAll() {
        let entities = fetchAll()
        
        for entity in entities {
            CoreDataTool.context.delete(entity)
        }
        
        if CoreDataTool.context.hasChanges {
            do {
                try CoreDataTool.context.save()
            } catch {
                print("Failed saving")
            }
        }
    }
    
    func fetchAll() -> [Student] {
        var students = [Student]()
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Student")
        
        do {
            students = try CoreDataTool.context.fetch(request) as! [Student]
            print(students)
        } catch {
            print("Faiiled Fetching")
        }
        
        return students
    }
}
